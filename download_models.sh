#!/bin/bash

mkdir tmp_downloaded_models
cd tmp_downloaded_models
while read m; do
  wget -nc  http://models.gazebosim.org/$m/model.tar.gz --accept gz -O "$m.tar.gz"
done <../extra_models.txt

for i in *
do
  tar -zvxf "$i"
done

cd ..
rm include/standard_models
mkdir include/standard_models
rm tmp_downloaded_models/*.tar.gz
find tmp_downloaded_models -name 'model.sdf' -exec sed -i '/<static>true<\/static>/,1 d' {} +
cp -vfR tmp_downloaded_models/* include/standard_models
rm -r tmp_downloaded_models
