How does this repo work? Not even I know.

# Todo

- [x] Create a plank like block of relevant proportions.
- [x] Create several of them, with random poses.
- [x] Have a depth camera and logic camera look at the objects, and produce data.
- [x] Create distracting objects.
- [x] Have the camera move around randomly.
- [x] Extract data automatically.
- [x] Script and automate this process, to generate a dataset.
- [ ] Write neural network for estimating pose information
- [ ] Get estimate accuracy > 95%
- [ ] Be able to estimate poses of multiple objects
- [ ] Be able to estimate the size and pose of arbitrary planks

ideas for increasing accuracy:

- [ ] Change input from depth data, to point clouds
- [ ] Add RGB data to input

# Build

```
docker build -t gazebo .
```

# Run

## Run serial batch job (best method so far)

This runs the simulation serially with a simple `for` loop. It runs the simulation as many
times as specified in the `batch_size` enviroment variable.

```
batch_size=30
for i in $(seq $batch_size); do docker run --rm -v ~/Downloads/dataset:/mnt/dataset gazebo; done
```

## Run headless

This runs the simulation once.

```
docker run --rm -v ~/Downloads/dataset:/mnt/dataset gazebo
```

## Run with GUI

This runs the simulation once, with a GUI. Ensure that `xhost` permissions are
set so that the docker container can access the host's X-server.

```
xhost +

docker run -it --rm \
-e DISPLAY=$DISPLAY \
-v ~/Downloads/dataset:/mnt/dataset \
-v /tmp/.X11-unix:/tmp/.X11-unix:ro \
gazebo gazebo --verbose lit_world.world
```

## Run parallel batch job

This runs the simulation in parallel as many times as specified by the
`batch_size` enviroment varaible. strangely, dispite running in parallel, using
more CPU than the serial batch run, this parallel method runs just as quickly.
Wierd. Consquently, I do not recommend using this method, over the serial
method, I am leaving it here, in case whatever misuse I'm applying can be
removed later.

This method requires the GNU Parallel application.

```
batch_size=20
seq $batch_size | parallel -j-2 "docker run --rm -v ~/Downloads/dataset:/mnt/dataset --name=plank-drop-container-{} gazebo"
```

# Other

In order to prevent having to re-download every single model for each docker
container, the `download_models.sh` script is included to allow you to download
a copy of the entire public gazebo model database in such a way that they will
be transferred to each new docker container created.

It will download the models specified in the `extra_models.txt` file.

Run it like this:

```
./download_models.sh
```

# Issues
