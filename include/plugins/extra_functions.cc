#include <gazebo/common/common.hh>

#include <random>
#include <list>
#include <string>
#include <sstream>
#include <uuid/uuid.h>


// Generates a double within the range low and high
double rand_range(double low, double high)
{
    double random_in_range = low +
       static_cast <double> (rand()) /
       (static_cast <double> ( RAND_MAX / (high - low))
    );

    return random_in_range;
}

// Creates pose data for planks
double drop_cord_neg () {return rand_range(-1.0, 1.0);}
double drop_cord_pos () {return rand_range(0.0, 4.0);}

// Creates position data for the cam
// z: above ground level
double cam_pos_z () {return rand_range(1.0, 7.0);}

// x,y: More or less anywhere
double cam_pos_xy () {return rand_range(-10.0, 10.0);}

// roll: not enough so the camera is upside down, but permuted
//       enough
double cam_roll () {return rand_range(-2.0, 2.0);}

// pitch: usually pointing at the ground, but not always
double cam_pitch () {return rand_range(-0.5, 2.0);}

// yaw: oriented based on which quadrant the camera is in
//      to point mostly towards 0,0,0
double cam_yaw (double cam_x, double cam_y)
{
    double range_low;
    double range_high;
    
    if (cam_x < 0 && cam_y < 0)
    {
        range_low = 0.0;
        range_high = 1.570796327;
    }
    else if (cam_x > 0 && cam_y < 0)
    {
        range_low = 1.570796327;
        range_high = 3.141592654;
    }
    else if (cam_x > 0 && cam_y > 0)
    {
        range_low = 3.141592654;
        range_high = 4.71238898;
    }
    else
    {
        range_low = 4.71238898;
        range_high = 6.283185307;
    }

    return rand_range(range_low, range_high);
}

// Joins a list of coordinates together, so they can be specified for the SDF pose
std::string join (std::list<double> cords, std::string join_str)
{
    std::stringstream joined;

    for (auto iter = cords.begin(); iter != cords.end(); iter++)
    {
        if (iter != cords.begin())
            joined << join_str;
        joined << std::to_string(*iter);
    }

    return joined.str();
}
std::string join (std::list<std::string> cords, std::string join_str)
{
    std::stringstream joined;

    for (auto iter = cords.begin(); iter != cords.end(); iter++)
    {
        if (iter != cords.begin())
            joined << join_str;
        joined << *iter;
    }

    return joined.str();
}

// Generates a UUID string
std::string guid ()
{
    uuid_t id;
    uuid_generate(id);

    char *id_string = new char[100];

    uuid_unparse(id, id_string);

    return std::string(id_string);
}

// Inserts a model with a random pose in the center of the scene.
sdf::SDFPtr drop_model (std::string model_name, std::string name_suffix)
{
    // Get the path to the model
    std::string model_file_path = gazebo::common::ModelDatabase::Instance()->GetModelFile("model://" + model_name);

    // Create an SDF object from a string
    sdf::SDFPtr modelSDF = sdf::readFile(model_file_path);

    // Get a pointer to the model element
    sdf::ElementPtr model_elm = modelSDF->Root()->GetElement("model");

    // Set the model name to be unique
    model_elm->GetAttribute("name")->SetFromString(model_name + name_suffix);

    // Set the model's pose to a random value
    std::string model_pose_string = join({
            drop_cord_neg(), drop_cord_neg(), drop_cord_pos(), 
            drop_cord_neg(), drop_cord_neg(), drop_cord_neg()
            }, " ");
    model_elm->GetElement("pose")->GetValue()->Set(model_pose_string);

    // Return the model
    return modelSDF;
}
