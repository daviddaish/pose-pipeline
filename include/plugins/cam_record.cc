#include "gazebo/common/common.hh"
#include "gazebo/physics/physics.hh"
#include <gazebo/sensors/sensors.hh>
#include <gazebo/rendering/rendering.hh>
#include <gazebo/gazebo.hh>
#include <iostream>
#include <string>
#include <fstream>
#include <signal.h>
#include <sys/types.h>
#include <unistd.h>        

#include "extra_functions.h"

#define DEPTH_SCALE_M 0.001

/////////////////////////////////////////////////////////////
namespace gazebo
{
class CamRecord : public ModelPlugin
{
    public: void Load (physics::ModelPtr _model, sdf::ElementPtr _sdf)
    {
        // Store the pointer to the model and world
        this->model = _model;
        this->world = _model->GetWorld();

        // Generate a UUID for this run
        this->uuid = guid();

        // Get the path for the root dir where the data is stored
        if (_sdf->HasElement("data_path"))
        {
            this->root_data_dir = _sdf->Get<std::string>("data_path");
        } else {
            gzerr << "Please specify where to store data with the <data_path> element.\n";
        }

        // Get and store pointers to the camera renderers and logic sensor
        // Colour camera's renderer (gazebo::rendering::CameraPtr)
        sensors::SensorPtr colour_sensor = sensors::get_sensor("colour");
        this->colour_camera = std::dynamic_pointer_cast
                <sensors::CameraSensor>(colour_sensor)->Camera();

        // Depth camera's renderer (gazebo::rendering::DepthCameraPtr)
        sensors::SensorPtr depth_sensor = sensors::get_sensor("depth");
        this->depth_camera = std::dynamic_pointer_cast<sensors::DepthCameraSensor>
                (depth_sensor)->DepthCamera();

        // Logical camera (gazebo::sensors::LogicalCameraSensorPtr)
        sensors::SensorPtr generic_logic_sensor = sensors::get_sensor("logic");
        this->logic_sensor = std::dynamic_pointer_cast<sensors::LogicalCameraSensor>
                (generic_logic_sensor);

        // Listen for new frames from the depth camera, passing on the pointer
        // to the camera, and the world
        this->newDepthFrameConn = this->depth_camera->ConnectNewDepthFrame(
                std::bind(&gazebo::CamRecord::OnNewDepthFrame, this
                )); 
    }

    public: void OnNewDepthFrame()
    {
        if (this->world->SimTime().Double() >= this->run_duration
                && this->world->SimTime().Double() <= (this->run_duration + 1))
        {
            // Define, and create directory to hold this run's data
            std::string run_data_dir = join(std::list<std::string> {
                    this->root_data_dir,
                    this->uuid},
                    "/");
            mkdir(run_data_dir.c_str(), 0777);

            // Get the depth map's dimensions, so we can allocate space for it
            unsigned int image_size = this->depth_camera->ImageWidth() *
                                      this->depth_camera->ImageHeight();

            // Get depth data as an array of floats
            const float *depth_data = this->depth_camera->DepthData();

            // Write depth data to a file
            std::string test_file_name = join(std::list<std::string> {
                        run_data_dir,
                        "depth_data.txt"},
                        "/"
                        );
            std::ofstream depth_data_file(test_file_name);
            for (unsigned int i = 0; i < image_size; i++)
            {
                depth_data_file << std::to_string(depth_data[i]) << "\n";
            }

            depth_data_file.close();

            // Get the colour camera's data
            this->colour_camera->SaveFrame(
                join(std::list<std::string> {
                    run_data_dir,
                    "colour_data.png"},
                    "/"
                    ));

            // Get the poses of planks and write to file
            auto logic_cam_data = this->logic_sensor->Image();

            std::ofstream pose_data_file(
                join(std::list<std::string> {
                    run_data_dir,
                    "pose_data.txt"},
                    "/"
                    ));

            for (int i = 0; i < logic_cam_data.model_size(); i++)
            {
                auto model = logic_cam_data.model(i);

                if (model.name().substr(0, 5) == "plank")
                {
                    auto position = model.pose().position();
                    auto orientation = model.pose().orientation();
                    
                    std::list<double> cords = {
                            position.x(), position.y(), position.z(),
                            orientation.x(), orientation.y(), orientation.z(),
                            orientation.w()
                            };
                    std::string pose_data = join(cords, ":");

                    pose_data_file << model.name() << ":" << pose_data << "\n";
                }
            }

            pose_data_file.close();

            // End the program
            this->world->Fini();
            auto this_pid = getpid();
            kill(this_pid, 3);
        }
        else if (this->world->SimTime().Double() > (this->run_duration + 1))
        {
            // End the program
            this->world->Fini();
            auto this_pid = getpid();
            kill(this_pid, 3);
        }
    }

    // Seconds to wait before collecting data and exiting
    private: double run_duration = 6;

    // Root directory where data is stored
    private: std::string root_data_dir;

    // Storing pointers to the model and world
    private: physics::ModelPtr model;
    private: physics::WorldPtr world;
    
    // Storing a uuid for this run
    private: std::string uuid;

    // Storing pointers to the cameras
    private: rendering::CameraPtr colour_camera;
    private: rendering::DepthCameraPtr depth_camera;
    private: sensors::LogicalCameraSensorPtr logic_sensor;

    // Making connections between the event of a new frame, and the functions
    // that handle that event
    private: event::ConnectionPtr newColourFrameConn;
    private: event::ConnectionPtr newDepthFrameConn;
};

GZ_REGISTER_MODEL_PLUGIN(CamRecord)
}
