#ifndef EXTRA_FUNCTIONS_H
#define EXTRA_FUNCTIONS_H

// Generates a double within the range low and high
double rand_range(double low, double high);

// Creates pose data for planks
double plank_cord_neg ();
double plank_cord_pos ();

// Creates position data for the cam
// z: above ground level
double cam_pos_z ();

// x,y: More or less anywhere
double cam_pos_xy ();

// roll: not enough so the camera is upside down, but permuted
//       enough
double cam_roll ();

// pitch: usually pointing at the ground, but not always
double cam_pitch ();

// yaw: oriented based on which quadrant the camera is in
//      to point mostly towards 0,0,0
double cam_yaw (double cam_x, double cam_y);

// Joins a list of coordinates together, so they can be specified for the SDF pose
std::string join (std::list<double> cords, std::string join_str);
std::string join (std::list<std::string> cords, std::string join_str);

// Generates a UUID string
std::string guid ();

// Inserts a model with a random pose in the center of the scene.
sdf::SDFPtr drop_model (std::string model_name, std::string name_suffix);

#endif
