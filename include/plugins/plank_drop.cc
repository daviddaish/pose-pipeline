#include <ignition/math/Pose3.hh>
#include "gazebo/physics/physics.hh"
#include "gazebo/common/common.hh"
#include "gazebo/gazebo.hh"
#include <iostream>
#include <string>
#include <fstream>
#include <streambuf>
#include <random>

#include "extra_functions.h"


//////////////////////////////////////////////////////////////
namespace gazebo
{
class PlankDrop : public WorldPlugin
{
    public: void Load(physics::WorldPtr _parent, sdf::ElementPtr /*_sdf*/)
    {
        // Seed randomness
        srand(static_cast <unsigned> (time(0)));

        // Adding planks
        for (int x = 0; x <= 10; x++)
        {
            // Generate the plank model in a pose ready for dropping
            sdf::SDFPtr plankSDF = drop_model("plank", std::to_string(x));

            // Insert the model
            _parent->InsertModelSDF(*plankSDF);
        }
        
        // Adding extra models
        // Get all acceptable extra models
        std::ifstream extramodel_file("extra_models.txt");
        std::vector<std::string> extramodel_names;

        std::string line;
        while (std::getline(extramodel_file, line))
        {
            extramodel_names.push_back(line);
        }
        
        // Insert extra models
        for (int x = 0; x <= 15; x++)
        {
            // Select a random model to use
            int rand_index = int(rand_range(0, extramodel_names.size()));
            std::string rand_model_name = extramodel_names[rand_index];

            // Generate the plank model in a pose ready for dropping
            sdf::SDFPtr extraSDF = drop_model(rand_model_name, std::to_string(x));

            // Insert the model
            _parent->InsertModelSDF(*extraSDF);
        }

        // Adding the camera
        // Insert the multi output camera
        std::string cam_file_path = common::ModelDatabase::Instance()->GetModelFile("model://multi-cam");
        sdf::SDFPtr camSDF = sdf::readFile(cam_file_path);
        sdf::ElementPtr cam = camSDF->Root()->GetElement("model");

        // Set the model's pose to random-ish values
        double cam_x = cam_pos_xy();
        double cam_y = cam_pos_xy();
        double set_cam_yaw = cam_yaw(cam_x, cam_y);
        std::list<double> cords = {
                cam_x, cam_y, cam_pos_z(),
                cam_roll(), cam_pitch(), set_cam_yaw
                };
        std::string cam_pose_string = join(cords, " ");
        cam->GetElement("pose")->GetValue()->Set(cam_pose_string);
 
        // Insert the camera
        _parent->InsertModelSDF(*camSDF);
    }
};


GZ_REGISTER_WORLD_PLUGIN(PlankDrop )
}

