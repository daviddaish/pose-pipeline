FROM ubuntu:16.04

RUN apt-get update
RUN apt-get install curl -y
RUN curl -sSL http://get.gazebosim.org | sh
RUN apt-get install -y libgazebo9-dev protobuf-compiler uuid-dev xvfb

ENV GAZEBO_PLUGIN_PATH=/home/plugins/build
ENV GAZEBO_MODEL_PATH=/home/models:/home/standard_models

ADD extra_models.txt /home/extra_models.txt
ADD include /home

WORKDIR /home/plugins/build
RUN cmake ../ && make

WORKDIR /home

# Here we run gzserver with xvfb, because gzserver needs an X-server's frame
# buffer in order to perorm rendering. Because we want to run this headlessly,
# we need to run xvfb, which creates a headless X-server framebuffer. Without
# an X-server to connect to, we won't be able to access any of the camera's,
# because the rendering object won't have started.

# To run headless:
CMD nohup Xvfb -shmem -screen 0 1280x1024x24 & DISPLAY=:0 gzserver lit_world.world


# Here, we're running it without xvfb, because we're using the host machine's
# X-server instance.

# To run with GUI:
# CMD gzserver --verbose lit_world.world
